import java.util.Scanner;

public class TestClass {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        int a, b, choice;

        System.out.print("\nEnter A:");
        a = reader.nextInt();

        System.out.print("\nEnter B:");
        b = reader.nextInt();

        System.out.print("\nChoose Options To Perform : ");
        System.out.print("\n1.Addition");
        System.out.print("\n2.Subtraction");
        System.out.print("\n3.Multiplication");
        System.out.print("\n4.Division");
        System.out.print("\n\nEnter your choice:");
        choice = reader.nextInt();
        switch (choice) {
            case 1:
                System.out.print("\nAddition of A + B is : " + add(a, b));
                break;
            case 2:
                System.out.print("\nSubtraction of A - B is : " + subtract(a, b));
                break;
            case 3:

                System.out.print("\nMultiplication of A * B is : " + multiply(a, b));
                break;
            case 4:
                if (b == 0) {
                    System.out.print("\nb cannot be 0(Zero) for Division operation");
                    break;
                } else {
                    System.out.print("\nDivision of A / B is : " + divide(a, b));
                    break;
                }
            default:
                System.out.print("\nInvalid Choice");
        }
    }


    static int add(int num1, int num2) {
        return (num1 + num2);
    }

    static int subtract(int num1, int num2) {
        return num1 - num2;
    }


    static int multiply(int num1, int num2) {
        return num1 * num2;
    }

    static int divide(int num1, int num2) {
        return num1 / num2;
    }


}